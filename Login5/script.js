const campoContrasena = document.getElementById("contrasena");
const icono = document.getElementById("icono");

icono.addEventListener("click", function () {
  if (campoContrasena.type === "password") {
    campoContrasena.type = "text";
  } else {
    campoContrasena.type = "password";
  }
});
